from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from .serializers import ProductoSerializer, PosicionSerializer, ZonaSerializer
from .models import Producto
from rest_framework.response import Response
from rest_framework import status

@api_view(['POST'])
def añadir_producto(request):
    data = JSONParser().parse(request)

    data['zona']['posicion'] = añadir_posicion(data['zona']['posicion'])['id']
    data['zona'] = añadir_zona(data['zona'])['id']

    producto = ProductoSerializer(data=data)

    if producto.is_valid():
        producto.save()
        return JsonResponse(producto.data, status=201)

    return JsonResponse(producto.errors, status=400)

@api_view(['GET'])
def mostrar_productos(request):
    data = Producto.objects.all()
    serializer = ProductoSerializer(data, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['GET'])
def detalle_producto(request, id):
    try:
        producto = Producto.objects.get(pk=id)
    except producto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = ProductoSerializer(producto)
    return JsonResponse(serializer.data, safe=False)

@api_view(['PUT'])
def update_producto(request, id):
    try:
        producto = Producto.objects.get(pk=id)
    except producto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = JSONParser().parse(request)

    serializer = ProductoSerializer(producto, data=data)

    if serializer.is_valid():
        serializer.save()
        return JsonResponse(serializer.data, status=status.HTTP_202_ACCEPTED)
    return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def añadir_zona(data):
    serializer = ZonaSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return serializer.data
    
def añadir_posicion(data):
    serializer = PosicionSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return serializer.data
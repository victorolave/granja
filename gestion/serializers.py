from select import select
from rest_framework import serializers
from .models import Posicion, Zona, Producto


class ProductoSerializer (serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'tipo', 'zona', 'cantidad', 'estado']

    def to_representation(self, instance):
        self.fields['zona'] = ZonaSerializer(read_only=True)
        return super(ProductoSerializer, self).to_representation(instance)


class ZonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zona
        fields = ['id', 'nombre', 'posicion']

    def to_representation(self, instance):
        self.fields['posicion'] = PosicionSerializer(read_only=True)
        return super(ZonaSerializer, self).to_representation(instance)


class PosicionSerializer (serializers.ModelSerializer):
    class Meta:
        model = Posicion
        fields = ['id', 'latitud', 'longitud']

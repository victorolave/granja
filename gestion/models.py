from django.db import models


class Posicion(models.Model):
    latitud = models.CharField(max_length=150)
    longitud = models.CharField(max_length=150)


class Zona(models.Model):
    nombre = models.CharField(max_length=150)
    posicion = models.ForeignKey(Posicion, on_delete=models.SET_NULL, null=True)


class Producto(models.Model):
    nombre = models.CharField(max_length=150)
    tipo = models.CharField(max_length=150)
    zona = models.ForeignKey(Zona, on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    estado = models.CharField(max_length=150)
from django.conf.urls import url
from gestion import views


urlpatterns = [
    url(r'productos/añadir', views.añadir_producto),
    url(r'productos/lista', views.mostrar_productos),
    url(r'productos/(?P<id>[0-9]+)/$', views.detalle_producto),
]
